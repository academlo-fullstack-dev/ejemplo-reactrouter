import {
  BrowserRouter as Router,
  Link,
  NavLink,
  Switch,
  Route
} from 'react-router-dom';

import Contact from './components/Contact';
import Home from './components/Home';
import Shop from './components/Shop';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <header className="App-header">
          <nav>
            <ul>
              <li>
                <NavLink to="/" activeStyle={{fontWeight: "bold", color: "red"}} exact >Inicio</NavLink>
              </li>
              <li>
                <NavLink to="/shop" activeStyle={{fontWeight: "bold", color: "red"}}>Tienda</NavLink>
              </li>
              <li>
                <NavLink to="/contact" activeStyle={{fontWeight: "bold", color: "red"}}>Contacto</NavLink>
              </li>
            </ul>
          </nav>
        </header>
        <Switch>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/shop">
            <Shop />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
